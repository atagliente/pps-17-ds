package controller

import model.SudokuGene
import org.scalatest.FunSuite

class BoardOperationTest extends FunSuite {


  test ("testBoardCompletionRateWithInitialBoard") {
    assert(BoardOperation.boardCompletionRateWithInitialBoard(DefaultBoard.FourByFour.testC.flatten.map(SudokuGene),
                                                              DefaultBoard.FourByFour.testC.map(_.map(SudokuGene).toArray).toArray) == 1)
    assert(BoardOperation.boardCompletionRateWithInitialBoard(DefaultBoard.FourByFour.testC.flatten.map(SudokuGene),
                                                              DefaultBoard.FourByFour.testNC.map(_.map(SudokuGene).toArray).toArray) < 1)
  }

  test ("testBlocksControl") {
    assert(BoardOperation.blocksControl(DefaultBoard.FourByFour.testC.map(_.map(SudokuGene))) == 1)
    assert(BoardOperation.blocksControl(DefaultBoard.FourByFour.testNC.map(_.map(SudokuGene))) < 1)
  }

  test ("valuesControl") {
    assert(BoardOperation.valuesControl(DefaultBoard.FourByFour.testC.head.map(SudokuGene).toArray, 1 to 4) == 1)
    assert(BoardOperation.valuesControl(DefaultBoard.FourByFour.testNC.head.map(SudokuGene).toArray, 1 to 4) < 1)
  }

}
