package controller.io

import java.nio.file.Files

import org.scalatest.{BeforeAndAfter, FunSuite}

class CSVManagerTest extends FunSuite with BeforeAndAfter {

  val FILE_NAME = "test.csv"
  val content = TableData(
    Some(List("h1", "h2")),
    List(
      List("c11", "c12"),
      List("c21", "c22")))

  def clean(): Unit = if(Files.exists(file.path)) Files.delete(file.path)

  val file = CSVFile(FILE_NAME)
  before {
    clean()
  }


  test("writeAndRead") {
    val m = CSVManager()
    m.write(file, content)
    val reading = m.read(file).get
    val readContent = TableData(reading.header, reading.rows)
    assert(readContent === content)
  }

  after {
    clean()
  }
}
