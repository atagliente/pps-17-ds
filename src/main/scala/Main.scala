import view.View

object Main extends App {
    val view = View()
    view.start()
}
