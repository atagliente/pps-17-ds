package view.panel

import java.awt.Color

import javax.swing.BorderFactory
import view.component.SudokuCell

import scala.swing.{GridPanel, Panel}

/**
 * Custom `scala.swing.Panel` that represents a Sudoku sub-grid, for instance in a classic Sudoku grid 9x9 this would be
 * a 3x3 sub-grid
 */
trait SudokuBoxPanel extends Panel {

  /**
   * Value of cells in this box
   * @return
   */
  def values(): Iterable[Int]

}

object SudokuBoxPanel {

  def apply(cells: Iterable[SudokuCell], sideSize: Int): SudokuBoxPanel = new SudokuBoxPanelImpl(cells, sideSize)

  class SudokuBoxPanelImpl(cells: Iterable[SudokuCell], sideSize: Int) extends GridPanel(sideSize, sideSize) with SudokuBoxPanel {
    cells.foreach(cell => contents += cell)
    border = BorderFactory.createLineBorder(Color.BLACK, 2)

    override def values(): Iterable[Int] = cells.collect { case sc: SudokuCell => sc.value() }
  }

}
