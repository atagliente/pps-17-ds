package view.panel

import controller.{Settings, _}
import model.{SudokuChromosome, SudokuGene}
import util.GridCheck._
import util.MatrixUtils._
import view.component.SudokuCell

import scala.swing.Dialog.Message
import scala.swing.event.ButtonClicked
import scala.swing.{BoxPanel, Button, Dialog, FlowPanel, Frame, Orientation}

trait SudokuMainPanel extends FlowPanel {

  def close(): Unit

}

object SudokuMainPanel {

  def apply(boardId: String, settings: Settings, callback: Controller => Unit): SudokuMainPanel = new SudokuMainPanelImpl(boardId, settings, callback)

  class SudokuMainPanelImpl(val boardId: String, val settings: Settings, val callback: Controller => Unit) extends SudokuMainPanel {
    private [this] val controller: Controller = Controller(settings)
    controller.addObserver(this.update)
    val GRID_SIDE_SIZE: Int = Math.pow(settings.boardSize, 0.5).toInt
    private [this] var chart = ChartFrame(boardId, 0, 0, 0)
    chart.show()


    private def splitter[S](collection: Iterable[S]): List[List[S]] = {
      SudokuUtils.splitInBlock(collection.toList)
    }

    private [this] val demo: List[List[Int]] = (settings.difficulty, settings.boardSize) match {
      case (Difficulty.CUSTOM, _) => List.fill(settings.boardSize)(List.fill(settings.boardSize)(0))
      case (_, 4) => DefaultBoard.FourByFour.board
      case (Difficulty.BASIC, 9) => DefaultBoard.NineByNine.basic
      case (Difficulty.SIMPLE, 9) => DefaultBoard.NineByNine.simple
      case (Difficulty.MEDIUM, 9) => DefaultBoard.NineByNine.medium
      case (Difficulty.HARD, 9) => DefaultBoard.NineByNine.hard
      case (Difficulty.EXTREME, 9) => DefaultBoard.NineByNine.extreme
      case _ => List.fill(settings.boardSize)(List.fill(settings.boardSize)(0))
    }

    private [this] var gridPanel = SudokuGridPanel(splitter(demo.flatten.map(x => SudokuCell(x))) map (i => SudokuBoxPanel(i, GRID_SIDE_SIZE)), GRID_SIDE_SIZE)

    private [this] var stats = StatsPanel(0, 0)

    private def containsDuplicate(line: List[Int]): Boolean = line.filter(_ > 0).distinct.size != line.count(_ > 0)

    private def check(v: List[SudokuGene]): Boolean = {
      isValid(
        v.map(_.allele),
        GRID_SIDE_SIZE*GRID_SIDE_SIZE,
        containsDuplicate,
        (toCheck, max) => toCheck.exists(g => g > max)
      )
    }

    private def drawGrid(currentGridCells: Iterable[SudokuCell]): Unit = {
      val values = currentGridCells.map(_.value()).toList
      val newGrid = getRows(values, settings.boardSize).map(_.map(i => SudokuCell(i)))
      val rowsWithErrors = linesWithError(getRows(values, settings.boardSize), containsDuplicate)
      val colsWithErrors = linesWithError(getColumns(values, settings.boardSize), containsDuplicate)
      if (colsWithErrors.nonEmpty || rowsWithErrors.nonEmpty) {
        (0 until settings.boardSize).foreach(r => {
          (0 until settings.boardSize).foreach(c => {
            if (rowsWithErrors.contains(r) || colsWithErrors.contains(c)) {
              newGrid(r)(c).error(true)
            }
          })
        })
      }
      if (gamePanel.contents.contains(gridPanel)) gamePanel.contents -= gridPanel
      gridPanel = SudokuGridPanel(splitter(newGrid.flatten) map(i => SudokuBoxPanel(i, GRID_SIDE_SIZE)), GRID_SIDE_SIZE)
      gamePanel.contents += gridPanel
      gamePanel.revalidate()
      gamePanel.repaint()
    }

    private [this] val startButton: Button = new Button("Start") {
      reactions += {
        case ButtonClicked(_) =>
          text match {
            case "Reset" => {
              text = "Start"
              drawGrid(demo.flatten.map(x => SudokuCell(x)))
              chart.clear()
            }
            case "Start" => {
              drawGrid(SudokuUtils.blockToList(gridPanel.values()).map(g => SudokuCell(g.allele)))
              if (check(SudokuUtils.blockToList(gridPanel.values()))) {
                enabled = false
                controller.startSolver(SudokuChromosome(SudokuUtils.blockToList(gridPanel.values())))
                stopButton.enabled = true
              } else {
                Dialog.showMessage(new Frame(), "Fix errors to start the Sudoku!", "Error!", Message.Error)
              }
            }
            case _ =>
          }
      }
    }

    private [this] val stopButton: Button = new Button("Stop") {
      reactions += {
        case ButtonClicked(_) =>
          startButton.enabled = true
          startButton.text = "Reset"
          controller.stopSolver()
          stopButton.enabled = false
      }
    }

    private [this] val commandPanel: BoxPanel = new BoxPanel(Orientation.Horizontal) {
      contents += startButton
      contents += stopButton
      contents += new Button("Show Settings") {
        reactions += {
          case ButtonClicked(_) =>
            Dialog.showMessage(new Frame(),
              "Difficulty: " + settings.difficulty +
                "\nCrossover function: " + settings.crossoverFunction +
                "\nMutation function: " + settings.mutationFunction +
                "\nSelection function: " + settings.selectionFunction +
                "\nElitism rate: " + settings.elitismRate +
                "\nMutation rate: " + settings.mutationRate +
                "\nStart population size: " + settings.populationSize)
        }
      }

      contents += new Button("Save chart") {
        reactions += {
          case ButtonClicked(_) =>
            chart.save()
            Dialog.showMessage(new Frame(), "#" + boardId + " saved!")
        }
      }

      contents += stats
    }

    private [this] val gamePanel: BoxPanel = new BoxPanel(Orientation.Vertical) {
      contents += commandPanel
      contents += gridPanel
    }

    contents += gamePanel

    private def finish(state: Status): Unit = {
      stopButton.reactions(ButtonClicked(stopButton))
      Dialog.showMessage(new Frame(),
        "Solution found in " + state.time + "ms and " + state.gen + " generations!\n" +
          "Parameters:" +
          "\npopulation size: " + settings.populationSize +
          "\nmutation rate: " + settings.mutationRate +
          "\nelitism rate: " + settings.elitismRate)
    }

    private def update(controller: Controller): Unit = {
      val state: Status = controller.status
      swing.Swing onEDT {
        drawGrid(state.leader.genes.toList.map(g => SudokuCell(g.allele)))
        if (check(state.leader.genes.toList)) finish(state)
        commandPanel.contents -= stats
        stats = StatsPanel(state.gen, state.time / 1000)
        commandPanel.contents += stats
        gamePanel.revalidate()
        gamePanel.repaint()
      }
      chart.update(state.gen.toInt, state.avg, state.leader.fitRate)
      if (state.leader.fitRate == 1) callback(controller)
    }

    override def close(): Unit = {
      chart.close()
      controller.stopSolver()
    }
  }

}
