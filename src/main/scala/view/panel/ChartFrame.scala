package view.panel

import de.sciss.chart
import de.sciss.chart.module.ChartFactories

trait ChartFrame extends chart.module.Charting {

  /**
   * Update chart data
   * @param a
   * @param b
   * @param c
   */
  def update(a: Int, b: Double, c: Double): Unit

  /**
   * Show chart
   */
  def show(): Unit

  /**
   * Save chart as .png file
   */
  def save(): Unit

  /**
   * Close chart
   */
  def close(): Unit

  /**
   * Clear chart data
   */
  def clear(): Unit

}

object ChartFrame {
  def apply(boardId: String, a: Int, b: Double, c: Double): ChartFrame = new ChartFrameImpl(boardId, a, b, c)

  class ChartFrameImpl(val boardId: String, val a: Int, val b: Double, val c: Double) extends ChartFrame {
    val ds = new org.jfree.data.category.DefaultCategoryDataset
    val chart = ChartFactories.LineChart(ds)
    chart.plot.getDomainAxis().setVisible(false)
    val title: String = "Chart: #" + boardId

    override def show(): Unit = chart.show(title)

    override def update(a: Int, b: Double, c: Double): Unit = {
      ds.addValue(b, new String("AVG"), a)
      ds.addValue(c, new String("FIT"), a)
    }

    override def save(): Unit = chart.saveAsPNG("chart_" + boardId + ".png")

    override def close(): Unit = {
      import view.component.CustomChart._
      chart.close(title)
    }

    override def clear(): Unit = ds.clear()

  }

}
