package view.panel

import controller.{Controller, SudokuUtils}
import javax.swing.BorderFactory
import model.SudokuChromosome
import util.Util
import view.component.{Popup, SudokuCell}

import scala.swing.event.ButtonClicked
import scala.swing.{Button, Dimension, FlowPanel, GridPanel, Label, ScrollPane}

trait ScorePanel extends FlowPanel {
  /**
   * Update content with controller data
   * @param controller
   */
  def update(controller: Controller): Unit
}

object ScorePanel {

  def apply(): ScorePanel = new ScorePanelImpl()

  class ScorePanelImpl extends ScorePanel {

    val controller = Controller(_root_.controller.Settings())

    border = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Scores")
    val mainPanel = new GridPanel(0, 3)

    var items: List[(SudokuChromosome, List[Int])] = List.empty
    override def update(controller: Controller): Unit = {
      items = controller.getSavedItems
      items.foreach(item => {
        val (chromosome, results) = item
        mainPanel.rows = mainPanel.rows + 1
        mainPanel.contents += new Button("Grid") {
          reactions += {
            case ButtonClicked(_) =>
              Popup("Result", SudokuGridPanel(SudokuUtils.splitInBlock(
                chromosome.genes.map(x => SudokuCell(x.allele)).toList) map (i =>
                SudokuBoxPanel(i, Util.powOfInt(chromosome.genes.size, 0.25))),
                Util.powOfInt(chromosome.genes.size, 0.25)))
          }
        }
        mainPanel.contents += new Label("Generations: " + results(0) + ", ")
        mainPanel.contents += new Label("Time: " + results(1) + " ms")
      })
    }

    this.update(controller)

    contents += new ScrollPane(mainPanel) {
      preferredSize = new Dimension(mainPanel.preferredSize.width + 50, if(items.size < 7) mainPanel.preferredSize.height else 230)
      verticalScrollBarPolicy = ScrollPane.BarPolicy.AsNeeded
    }
  }

}
