package view.panel

import model.SudokuGene

import scala.swing.{Dimension, GridPanel, Panel}

/**
 * Custom `scala.swing.Panel` that represents a Sudoku grid
 */
trait SudokuGridPanel extends Panel {
  /**
   * Value of each cell in this grid
   * @return
   */
  def values(): List[List[SudokuGene]]
}

object SudokuGridPanel {
  def apply(subBoxes: Iterable[Panel], sideSize: Int): SudokuGridPanel = new SudokuGridPanelImpl(subBoxes, sideSize)

  class SudokuGridPanelImpl(subBoxes: Iterable[Panel], sideSize: Int) extends GridPanel(sideSize, sideSize) with SudokuGridPanel {
    subBoxes.foreach(box => contents += box)
    preferredSize = new Dimension(600, 500)

    override def values(): List[List[SudokuGene]] = subBoxes.map(_.asInstanceOf[SudokuBoxPanel].values().map(v => SudokuGene(v)).toList).toList
  }

}
