package view.panel

import controller.Settings
import view.frame.SudokuFrame

import scala.swing.{BoxPanel, FlowPanel, Orientation}

trait IntroPanel extends FlowPanel

object IntroPanel {

  def apply(): IntroPanel = new IntroPanelImpl()

  class IntroPanelImpl extends IntroPanel {

    val scorePanel = ScorePanel()

      contents += new BoxPanel(Orientation.Vertical) {
        contents += SettingsPanel((s: Settings) => {
          SudokuFrame(s, scorePanel.update)
        })
        contents += scorePanel
      }

  }

}
