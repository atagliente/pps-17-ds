package view.panel

import scala.swing.{FlowPanel, Label}

trait StatsPanel extends FlowPanel

object StatsPanel {

  def apply(generation: Long, time: Long): StatsPanel = new StatsPanelImpl(generation, time)

  class StatsPanelImpl(generation: Long, time: Long) extends StatsPanel {
    contents += new Label(" Current generation: " + generation + " ")
    contents += new Label(" Elapsed time: " + time + " sec")
  }

}
