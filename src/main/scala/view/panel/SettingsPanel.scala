package view.panel

import controller.Functions.{Crossover, Mutation, Selection}
import controller.{Difficulty, Functions, Settings}
import javax.swing.BorderFactory
import view.component.CustomLabel

import scala.swing.Dialog.Message
import scala.swing.event.{ButtonClicked, SelectionChanged}
import scala.swing.{Alignment, BoxPanel, Button, ComboBox, Dialog, FlowPanel, Frame, GridPanel, Orientation, TextField}

trait SettingsPanel extends FlowPanel

object SettingsPanel {

  def apply(callback: (Settings) => Unit): SettingsPanel = new SettingsPanelImpl(callback)

  class SettingsPanelImpl(callback: (Settings) => Unit) extends SettingsPanel {

    val DIFFICULTY_LABEL: String = "Difficulty: "
    val CROSSOVER_FUNCTION_LABEL: String = "Crossover function: "
    val MUTATION_FUNCTION_LABEL: String = "Mutation function: "
    val SELECTION_FUNCTION_LABEL: String = "Selection function: "
    val SUDOKU_SIZE_LABEL: String = "Sudoku size: "
    val POPULATION_LABEL: String = "Population: "
    val ELITISM_RATE_LABEL: String = "Elitism rate: "
    val MUTATION_RATE_LABEL: String = "Mutation rate: "
    val BUTTON_LABEL: String = "Create"
    val ERROR_LABEL: String = "Input Error!"
    val DEFAULT_POP_NUMBER = 100
    val DEFAULT_ELITISM_RATE = 0.01
    val DEFAULT_MUTATION_RATE = 0.05

    var populationNumber: Int = DEFAULT_POP_NUMBER
    var elitismRate: Double = DEFAULT_ELITISM_RATE
    var mutationRate: Double = DEFAULT_MUTATION_RATE

    val sudokuSize: ComboBox[String] = new ComboBox(Seq("9x9", "4x4"))
    listenTo(sudokuSize.selection)
    reactions += {
      case SelectionChanged(`sudokuSize`) => difficultySelection.enabled = sudokuSize.selection.item == "9x9"
    }

    val difficultySelection: ComboBox[String] = new ComboBox(
      Seq(
        Difficulty.BASIC.toString,
        Difficulty.SIMPLE.toString,
        Difficulty.MEDIUM.toString,
        Difficulty.HARD.toString,
        Difficulty.EXTREME.toString,
        Difficulty.CUSTOM.toString
      )
    )

    val crossoverFunctionsSelection: ComboBox[String] = new ComboBox(
      Seq(
        Functions.Crossover.SINGLE_BLOCK_CROSSOVER.toString,
        Functions.Crossover.DOUBLE_BLOCKS_CROSSOVER.toString
      )
    )
    val mutationFunctionsSelection: ComboBox[String] = new ComboBox(
      Seq(
        Functions.Mutation.DOUBLE_CELL_MUTATION.toString,
        Functions.Mutation.SINGLE_CELL_MUTATION.toString,
        Functions.Mutation.THREE_SWAP_MUTATION.toString
      )
    )
    val selectionFunctionsSelection: ComboBox[String] = new ComboBox(
      Seq(
        Functions.Selection.FIT_RATE_SELECTION.toString,
        Functions.Selection.RANDOM_SELECTION.toString,
        Functions.Selection.ROULETTE_WHEEL_SELECTION.toString
      )
    )
    val populationFieldInput = new TextField(populationNumber.toString)
    val elitismFieldInput = new TextField(elitismRate.toString)
    val mutationFieldInput = new TextField(mutationRate.toString)

    border = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Settings")
    contents += new BoxPanel(Orientation.Vertical) {
      contents += new GridPanel(8, 2) {
        contents += CustomLabel(SUDOKU_SIZE_LABEL, Alignment.Right)
        contents += sudokuSize
        contents += CustomLabel(DIFFICULTY_LABEL, Alignment.Right)
        contents += difficultySelection
        contents += CustomLabel(CROSSOVER_FUNCTION_LABEL, Alignment.Right)
        contents += crossoverFunctionsSelection
        contents += CustomLabel(MUTATION_FUNCTION_LABEL, Alignment.Right)
        contents += mutationFunctionsSelection
        contents += CustomLabel(SELECTION_FUNCTION_LABEL, Alignment.Right)
        contents += selectionFunctionsSelection
        contents += CustomLabel(POPULATION_LABEL, Alignment.Right)
        contents += populationFieldInput
        contents += CustomLabel(ELITISM_RATE_LABEL, Alignment.Right)
        contents += elitismFieldInput
        contents += CustomLabel(MUTATION_RATE_LABEL, Alignment.Right)
        contents += mutationFieldInput
      }

      contents += new Button(BUTTON_LABEL) {
        reactions += {
          case ButtonClicked(_) =>
            try {
              elitismRate = elitismFieldInput.text.toDouble
              mutationRate = mutationFieldInput.text.toDouble
              populationNumber = populationFieldInput.text.toInt
              if (elitismRate > 1 || mutationRate > 1) throw new NumberFormatException()
              val settings = controller.Settings(
                elitismRate = elitismRate,
                mutationRate = mutationRate,
                populationSize = populationNumber,
                boardSize = sudokuSize.selection.item match {
                  case "4x4" => 4
                  case "9x9" => 9
                  case _ => 9
                },
                difficulty = Difficulty.withName(difficultySelection.selection.item),
                selectionFunction = Selection.withName(selectionFunctionsSelection.selection.item),
                mutationFunction = Mutation.withName(mutationFunctionsSelection.selection.item),
                crossoverFunction = Crossover.withName(crossoverFunctionsSelection.selection.item)
              )
              callback(settings)
            } catch {
              case _: Throwable =>
                Dialog.showMessage(new Frame(), ERROR_LABEL, "Error", Message.Error)
            }
        }
      }
    }
  }

}
