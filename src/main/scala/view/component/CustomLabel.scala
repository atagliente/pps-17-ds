package view.component

import scala.swing.{Alignment, Label}

object CustomLabel {

  def apply(text: String, alignment: Alignment.Value): CustomLabel = new CustomLabel(text, alignment)

  class CustomLabel(val value: String, val alignment: Alignment.Value) extends Label(value) {
    horizontalAlignment = alignment
  }

}
