package view.component

import java.awt.Frame

import de.sciss.chart.CategoryChart

object CustomChart {

  implicit class CustomChart(chart: CategoryChart) {
    /**
     * Find and remove fragment
     * @param title of fragment to remove
     */
    def close(title: String): Unit = Frame.getFrames.find(_.getTitle == title).getOrElse(new Frame).dispose()
  }

}
