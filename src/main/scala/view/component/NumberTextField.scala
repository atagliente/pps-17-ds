package view.component

import javax.swing.text.{AbstractDocument, AttributeSet, DocumentFilter}

import scala.swing.TextField

/**
 * Custom `scala.swing.TextField` that works with integer of one digit greater than 0
 */
object NumberTextField {

  def apply(value: Int): NumberTextField = new NumberTextField(value)

  class NumberTextField(value: Int) extends TextField(if (value > 0) value.toString else "") {

    object Filter extends DocumentFilter {

      override def replace(fb: DocumentFilter.FilterBypass, offset: Int, length: Int, text: String, attrs: AttributeSet): Unit = {
        val currentLen = fb.getDocument.getLength
        if (currentLen + text.length <= 1 && Character.isDigit(text.charAt(0))) super.replace(fb, offset, length, text, attrs)
      }
    }

    peer.getDocument.asInstanceOf[AbstractDocument].setDocumentFilter(Filter)

  }


}
