package view.component

import scala.swing.{Frame, MainFrame, Panel, SimpleSwingApplication}

/**
 * Popup frame
 */
object Popup {

  def apply(name: String, content: Panel): Popup = new Popup(name, content)

  class Popup(name: String, content: Panel) extends SimpleSwingApplication {
    override def top: Frame = new MainFrame {
      title = name
      contents = content
      resizable = false

      override def closeOperation(): Unit = {}

      pack()
      centerOnScreen()
      open()
    }

    this.top
  }

}
