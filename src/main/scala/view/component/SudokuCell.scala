package view.component

import java.awt.{Color, Dimension}

import javax.swing.BorderFactory
import view.component.NumberTextField.NumberTextField

import scala.swing.{Alignment, Font}

trait SudokuCell extends NumberTextField {
  /**
   * Set error
   * @param enable
   */
  def error(enable: Boolean): Unit

  /**
   *
   * @return cell value
   */
  def value(): Int
}

object SudokuCell {

  def apply(value: Int, error: Boolean = false): SudokuCell = new SudokuCellImpl(value, error)

  class SudokuCellImpl(value: Int, errorFlag: Boolean) extends NumberTextField(value) with SudokuCell {
    font = Font("Verdana", Font.Style.Plain, 20)
    border = BorderFactory.createLineBorder(Color.BLACK, 1)
    background = Color.WHITE
    opaque = true
    horizontalAlignment = Alignment.Center
    val dimension = new Dimension(30, 30)
    preferredSize = dimension
    minimumSize = dimension
    maximumSize = dimension
    error(errorFlag)

    override def error(enable: Boolean): Unit = if (enable) background = Color.pink else background = Color.WHITE

    override def value(): Int = try {
      text.toInt
    } catch {
      case _: NumberFormatException => 0
    }
  }

}
