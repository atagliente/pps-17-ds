package view

import view.frame.IntroFrame

trait View {

  def start(): Unit

  def close(): Unit

}

object View {
  def apply(): View = new ViewImpl()

  class ViewImpl() extends View {

    override def start(): Unit = IntroFrame.top

    override def close(): Unit = ???
  }

}