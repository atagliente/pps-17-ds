package view.frame

import util.Strings
import view.panel.IntroPanel

import scala.swing.{Frame, MainFrame, SimpleSwingApplication}

object IntroFrame extends SimpleSwingApplication {

  override def top: Frame = new MainFrame {
    title = Strings.INTRO_FRAME_TITLE

    contents = IntroPanel()

    pack()
    resizable = false
    centerOnScreen()
    open()
  }

}
