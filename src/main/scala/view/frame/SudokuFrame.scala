package view.frame

import controller.{Controller, Settings}
import view.panel.SudokuMainPanel

import scala.swing.{Frame, MainFrame, SimpleSwingApplication}

object SudokuFrame {

  def apply(settings: Settings, callback: Controller => Unit): SudokuFrame = new SudokuFrame(settings, callback)

  class SudokuFrame(settings: Settings, callback: Controller => Unit) {

    object SwingFrame extends SimpleSwingApplication {

      override def top: Frame = new MainFrame {
        title = "Board: #" + this.hashCode()

        validate()
        repaint()
        val panel = SudokuMainPanel(this.hashCode().toString, settings, callback)
        contents = panel

        override def closeOperation(): Unit = panel.close()


        resizable = false
        pack()
        centerOnScreen()
        open()
      }
    }
    SwingFrame.top

  }

}
