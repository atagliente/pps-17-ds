package view.frame

import view.panel.ScorePanel

import scala.swing.{Frame, MainFrame, SimpleSwingApplication}

object ScoreFrame extends SimpleSwingApplication {

  override def top: Frame = new MainFrame {
    title = "Scores"

    contents = ScorePanel()

    resizable = false
    pack()
    centerOnScreen()
    open()
  }

}
