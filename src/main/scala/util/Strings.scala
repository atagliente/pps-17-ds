package util

object Strings {

  val SAVE_FILE_PATH: String = "solutions.csv"
  val CSV_HEADER: (String, String, String) = ("Grid","Generations","Time")
  val INTRO_FRAME_TITLE = "Game settings"

}
