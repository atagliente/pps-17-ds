package util

trait Observable[S] {
  this: S =>

  private var observers: List[S => Unit] = Nil

  /**
   * Add observer to observers collection
   * @param observer to add
   */
  def addObserver(observer: S => Unit): Unit = observers = observer :: observers

  /**
   * Notify collected observers
   */
  def notifyObservers(): Unit = observers.foreach(_.apply(this))

}
