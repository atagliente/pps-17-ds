package util

object Util {

  def powOfInt(a: Int, b: Double) : Int = Math.pow(a.toDouble, b.toDouble).toInt

}
