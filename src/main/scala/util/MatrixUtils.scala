package util

object MatrixUtils {

  /**
   * With matrix represented as list, this returns one list per row
   * @param all matrix as list
   * @param side matrix side size
   * @tparam S
   * @return list of rows
   */
  def getRows[S](all: List[S], side: Int): List[List[S]] = {
    var rows: List[List[S]] = List.empty
    val splitSize, iterations = side
    var i = 0
    var start = 0
    while (i < iterations) {
      rows = rows :+ all.slice(start, splitSize + start)
      start += splitSize
      i += 1
    }
    rows
  }

  /**
   * With matrix represented as list, this returns one list per column
   * @param all matrix as list
   * @param side matrix side size
   * @tparam S
   * @return list of columns
   */
  def getColumns[S](all: List[S], side: Int): List[List[S]] = getRows(all, side).transpose

}
