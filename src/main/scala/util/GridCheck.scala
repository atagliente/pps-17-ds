package util

object GridCheck {

  /**
   * Checks if the grid is valid
   * @param grid to check
   * @param gridSide grid side size
   * @param containsDuplicate duplicate checking strategy
   * @param respectRange range checking strategy
   * @return true if the grid is valid
   */
  def isValid(grid: List[Int],
              gridSide: Int,
              containsDuplicate: List[Int] => Boolean,
              respectRange: (List[Int], Int) => Boolean
             ): Boolean = {

    import util.MatrixUtils._
    (0 until gridSide).foreach(index => {
      val rows = getRows(grid, gridSide)(index)
      val cols = getColumns(grid, gridSide)(index)
      if (containsDuplicate(rows) || containsDuplicate(cols)) {
        return false
      }
    })
    !respectRange(grid, gridSide)
  }

  /**
   * Finds lines with errors
   * @param matrix
   * @param containsDuplicate duplicate checking strategy
   * @return incorrect lines
   */
  def linesWithError(matrix: List[List[Int]], containsDuplicate: List[Int] => Boolean): List[Int] = {
    var incorrectLines: List[Int] = List()
    val sideSize = matrix(0).length
    (0 until sideSize).foreach(index => {
      val line = matrix(index)
      if (containsDuplicate(line)) {
        incorrectLines = incorrectLines :+ index
      }
    })
    incorrectLines
  }
}
