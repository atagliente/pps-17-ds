package controller

import controller.Functions.{Crossover, Mutation, Selection}
import model.{FittedSudokuChromosome, SudokuChromosome, SudokuGene}
import util.Util

trait Solver {

  /**
   * Start solver execution
   */
  def start(): Unit

  /**
   * Stop solver execution
   */
  def stop(): Unit

}

trait SudokuSolver extends Solver

object SudokuSolver {

  def apply(controller: Controller, toSolve: SudokuChromosome, settings: Settings): Solver = new SudokuSolverImpl(controller, toSolve, settings)

  class SudokuSolverImpl(controller: Controller, toSolve: SudokuChromosome, settings: Settings) extends SudokuSolver {

    @volatile private[this] var _isRun = false
    private[this]var startTime: Long = 0

    private def stopwatch_start: () => Unit = () => startTime = System.currentTimeMillis()
    private def stopwatch_elapsedTime: () => Long = () => System.currentTimeMillis() - startTime

    override def start(): Unit = {
      import GeneticEngine._
      import GeneticEngineImplicits._
      new Thread {
        override def run(): Unit = {
          _isRun = true
          val initialBoard: List[SudokuGene] = toSolve.genes.toList
          val initialBoardWithBlock = SudokuUtils.splitInBlock(initialBoard)

          def genFitPop: Int => List[FittedSudokuChromosome] =
            (size: Int) => List.fill(size)(FittedSudokuChromosome(SudokuUtils
              .generateRandomSudokuChromosomeWithBlock(initialBoardWithBlock,
                Util.powOfInt(initialBoard.size, 0.25)).genes,
              0.0))

          var gen: Int = 0
          var fitP = fitPopulation(genFitPop(settings.populationSize)) //Engine.fitPopulation(genFitPop(Settings.populationSize))
          var leader: FittedSudokuChromosome = fitP.head
          val availablePosByBlock = SudokuUtils.availablePositionByBlock(initialBoardWithBlock, 2)
          stopwatch_start()

          val selectionStrategy: ((List[FittedSudokuChromosome], FittedSudokuChromosome, Double) =>
            FittedSudokuChromosome, Double) = settings.selectionFunction match {
            case Selection.RANDOM_SELECTION => (SelectionFunctions.randomSelection, 0.0)
            case Selection.ROULETTE_WHEEL_SELECTION => (SelectionFunctions.rouletteWheel, 0.0)
            case Selection.FIT_RATE_SELECTION => (SelectionFunctions.byFitRate, settings.maximumOffset)
            case _ => (SelectionFunctions.randomSelection, 0.0)
          }

          val crossoverStrategy: (FittedSudokuChromosome, FittedSudokuChromosome) => FittedSudokuChromosome =
            settings.crossoverFunction match {
              case Crossover.SINGLE_BLOCK_CROSSOVER => CrossFunctions.singleBlock
              case Crossover.DOUBLE_BLOCKS_CROSSOVER => CrossFunctions.doubleBlocks
              case _ => CrossFunctions.singleBlock
            }

          val mutationStrategy: (Map[Int, List[Int]], List[FittedSudokuChromosome], Double) => List[FittedSudokuChromosome] =
            settings.mutationFunction match {
              case Mutation.SINGLE_CELL_MUTATION => MutationFunctions.simpleCellMutation
              case Mutation.DOUBLE_CELL_MUTATION => MutationFunctions.doubleCellsMutation
              case Mutation.THREE_SWAP_MUTATION => MutationFunctions.threeSwapMutation
              case _ => MutationFunctions.simpleCellMutation
            }
          while (_isRun && leader.fitRate < 1) {
            gen = gen + 1
            if (stopwatch_elapsedTime() / 1000 % 120 == 0 && (fitP.map(_.fitRate).sum / fitP.size) < 0.95) {
              fitP = genFitPop(fitP.size)
            }
            val elitismPop = Engine.elitism(fitP, settings.elitismRate)
            if ((gen % 3000) == 0) {
              println("RECREATE POPULATION!!!")
              fitP = fitP.take((fitP.size * 0.5).toInt) ::: fitPopulation(genFitPop((fitP.size * 0.5).toInt)) //Engine.fitPopulation(genFitPop((fitP.size * 0.5).toInt))
            }
            val elapsedDecMillis = stopwatch_elapsedTime() / 10
            val (function, offset) = selectionStrategy

            val sel: List[(FittedSudokuChromosome, FittedSudokuChromosome)] = selection(fitP, function, offset)
            val cro = crossover(sel, crossoverStrategy) //Engine.crossover(sel, crossoverStrategy)
            val mut = mutationStrategy(availablePosByBlock, cro, settings.mutationRate)
            fitP = fitPopulation(mut).splitAt((fitP.length * settings.elitismRate).toInt)._2 ::: elitismPop._1
            leader = fitP.maxBy(_.fitRate)(Ordering.Double.TotalOrdering)
            if (elapsedDecMillis % 5 == 0 || leader.fitRate == 1) {
              controller.status = Status(leader, fitP.map(_.fitRate).sum / fitP.size, gen, stopwatch_elapsedTime())
            }
          }
        }
      }.start()
    }

    override def stop(): Unit = _isRun = false
  }

}
