package controller

import controller.io.{CSVFile, CSVManager, TableData}
import model.{SudokuChromosome, SudokuGene}
import util.{Observable, Strings}

trait Controller extends Observable[Controller] {

  /**
   * Start to solve the Sudoku (in a different thread)
   *
   * @param toUpdate { @link model.Chromosome}
   */
  def startSolver(toUpdate: SudokuChromosome): Unit

  /**
   * Stop solver
   */
  def stopSolver(): Unit

  /**
   * Get the solving status
   *
   * @return status
   */
  def status: Status

  /**
   * Set status
   *
   * @return
   */
  def status_=(s: Status): Status

  /**
   * Save status on file
   *
   * @param status
   */
  def saveResult(status: Status): Unit

  /**
   * Get results saved on file, for example a couple with Chromosome and generations/time used to solve the Sudoku
   *
   * @return List of tuples of { @link model.Chromosome} and a List of Int
   */
  def getSavedItems: List[(SudokuChromosome, List[Int])]

}

object Controller {

  def apply(settings: Settings): Controller = new ControllerImpl(settings)

  class ControllerImpl(val settings: Settings) extends Controller {

    private[this] var _solver: Solver = _
    @volatile private[this] var _status: Status = _

    override def status: Status = synchronized {
      _status
    }

    override def status_=(s: Status): Status = {
      _status = s
      printStatus(_status, settings.mutationRate, settings.populationSize)
      if (_status.leader.fitRate == 1) {
        println("\n\nSOLUTION: \n")
        SudokuUtils.printSudokuBoard(_status.leader)
        saveResult(_status)
      }
      notifyObservers()
      _status
    }

    override def startSolver(toUpdate: SudokuChromosome): Unit = {
      _solver = SudokuSolver(this, toUpdate, settings)
      _solver.start()
    }

    def printStatus(status: Status, mutationRate: Double, populationSize: Int): Unit = {
      println("\n\nGEN: " + status.gen + " TIME: " + status.time / 1000 + " sec" +
        "\n\tPOPULATION: " + populationSize +
        "\n\tMUTATION RATE: " + mutationRate +
        "\n\tFIT AVG: " + status.avg +
        "\n\tLEADER FIT RATE: " + status.leader.fitRate)
    }

    override def getSavedItems: List[(SudokuChromosome, List[Int])] =
      CSVManager().read(CSVFile(Strings.SAVE_FILE_PATH)) match {
        case Some(data) => data.rows.flatMap(line =>
          Map(
            SudokuChromosome(line(0).split("\\s+").toList.map(_.toInt).map(SudokuGene))
              ->
              List(line(1).toInt, line(2).toInt)
          )
        )
        case None => List.empty
      }

    override def saveResult(status: Status): Unit = {
      val genes = status.leader.genes
      val writableGenes: String = genes.map(_.allele.toString).mkString(" ")
      val header = Strings.CSV_HEADER.productIterator.toList.map(_.toString)
      CSVManager().write(
        CSVFile(Strings.SAVE_FILE_PATH),
        TableData(
          Some(header),
          List(List(writableGenes, status.gen.toString, status.time.toString))
        )
      )
    }

    override def stopSolver(): Unit = _solver.stop()

  }


}
