package controller

import java.lang.Math._

import model.{FittedSudokuChromosome, SudokuChromosome, SudokuGene}

import scala.util.Random

object CrossFunctions {

  /**
    *
    *  Takes a blocks of genes from parentA and other blocks from parentB randomly and create a new Chromosome
    *
    * @param parentA
    * @param parentB
    * @return
    */
  def singleBlock(parentA:FittedSudokuChromosome, parentB:FittedSudokuChromosome): FittedSudokuChromosome = {
    val cp:Int = Random.nextInt(pow(parentA.genes.size, 0.5).toInt)
    val parA:List[List[SudokuGene]] = SudokuUtils.splitInBlock(parentA.genes.toList)
    val parB:List[List[SudokuGene]] = SudokuUtils.splitInBlock(parentB.genes.toList)
    val offspring:Array[List[SudokuGene]] = parA.toArray
    offspring(cp) = parB(cp)
    val board:List[SudokuGene] = SudokuUtils.blockToList(offspring.toList)
    FittedSudokuChromosome(SudokuChromosome(board).genes, 0.0)
  }

  /**
    *
    * Takes two blocks of genes from parentB and other blocks from parentA randomly and create a new Chromosome
    *
    * @param parentA
    * @param parentB
    * @return
    */
  def doubleBlocks(parentA:FittedSudokuChromosome, parentB:FittedSudokuChromosome): FittedSudokuChromosome = {
    singleBlock(singleBlock(parentA, parentB), parentB)
  }

}