package controller

/**
 *  Contains some default board for demo purpose
 */
object DefaultBoard {

  object NineByNine {
    val basic: List[List[Int]] = List(
      List(0, 5, 0, 3, 0, 6, 0, 0, 7),
      List(0, 0, 0, 0, 8, 5, 0, 2, 4),
      List(0, 9, 8, 4, 2, 0, 6, 0, 3),
      List(9, 0, 1, 0, 0, 3, 2, 0, 6),
      List(0, 3, 0, 0, 0, 0, 0, 1, 0),
      List(5, 0, 7, 2, 6, 0, 9, 0, 8),
      List(4, 0, 5, 0, 9, 0, 3, 8, 0),
      List(0, 1, 0, 5, 7, 0, 0, 0, 2),
      List(8, 0, 0, 1, 0, 4, 0, 7, 0)
    )

    val simple: List[List[Int]] = List(
      List(0, 6, 0, 0, 8, 0, 4, 2, 0),
      List(0, 1, 5, 0, 6, 0, 3, 7, 8),
      List(0, 0, 0, 4, 0, 0, 0, 6, 0),
      List(1, 0, 0, 6, 0, 4, 8, 3, 0),
      List(3, 0, 6, 0, 1, 0, 7, 0, 5),
      List(0, 8, 0, 3, 5, 0, 0, 0, 0),
      List(8, 3, 0, 9, 4, 0, 0, 0, 0),
      List(0, 7, 2, 1, 3, 0, 9, 0, 0),
      List(0, 0, 9, 0, 2, 0, 6, 1, 0))

    val medium: List[List[Int]] = List(
      List(0, 0, 0, 1, 7, 5, 0, 0, 6),
      List(0, 3, 0, 0, 6, 0, 1, 2, 0),
      List(0, 0, 7, 0, 0, 2, 0, 0, 0),
      List(0, 0, 0, 0, 0, 0, 3, 7, 0),
      List(7, 0, 0, 0, 4, 0, 0, 0, 1),
      List(0, 8, 4, 0, 0, 0, 0, 0, 0),
      List(0, 0, 0, 8, 0, 0, 5, 0, 0),
      List(0, 9, 2, 0, 5, 0, 0, 8, 0),
      List(8, 0, 0, 3, 2, 6, 0, 0, 0))

    val hard: List[List[Int]] = List(
      List(0, 0, 0, 0, 9, 0, 0, 0, 1),
      List(0, 4, 0, 3, 0, 7, 0, 0, 0),
      List(0, 7, 0, 5, 0, 0, 0, 9, 0),
      List(0, 5, 3, 0, 0, 0, 0, 0, 0),
      List(0, 0, 0, 0, 0, 0, 4, 2, 0),
      List(0, 0, 1, 0, 0, 0, 0, 6, 0),
      List(0, 0, 0, 9, 0, 0, 0, 0, 2),
      List(3, 0, 0, 8, 0, 0, 0, 0, 7),
      List(0, 8, 7, 0, 3, 0, 0, 1, 6))

    val extreme: List[List[Int]] = List(
      List(0, 2, 0, 0, 0, 0, 0, 0, 0),
      List(4, 5, 0, 2, 3, 0, 0, 0, 0),
      List(8, 0, 0, 0, 0, 0, 0, 0, 9),
      List(0, 4, 0, 6, 0, 0, 0, 7, 3),
      List(0, 0, 9, 0, 0, 1, 0, 0, 0),
      List(0, 0, 0, 0, 0, 0, 4, 0, 0),
      List(0, 0, 8, 0, 0, 0, 0, 0, 6),
      List(0, 0, 0, 5, 0, 0, 1, 0, 0),
      List(0, 7, 3, 0, 2, 0, 0, 0, 8))
  }

  object FourByFour extends Enumeration {
    val board: List[List[Int]] = List(
      List(0, 0, 4, 0),
      List(1, 0, 0, 0),
      List(0, 2, 0, 0),
      List(0, 0, 0, 3)
    )

    val testC: List[List[Int]] = List(
      List(1, 2, 3, 4),
      List(3, 4, 2, 1),
      List(2, 1, 4, 3),
      List(4, 3, 1, 2)
    )

    val testNC: List[List[Int]] = List(
      List(1, 2, 1, 4),
      List(2, 2, 4, 1),
      List(3, 4, 1, 2),
      List(4, 1, 2, 3)
    )
  }

}
