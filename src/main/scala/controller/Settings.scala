package controller

import controller.Difficulty.Difficulty
import controller.Functions.Crossover.Crossover
import controller.Functions.Mutation.Mutation
import controller.Functions.Selection.Selection

/**
 * Simulation parameters
 * @param elitismRate
 * @param mutationRate
 * @param maximumOffset
 * @param populationSize
 * @param boardSize
 * @param difficulty
 * @param selectionFunction
 * @param mutationFunction
 * @param crossoverFunction
 */
case class Settings(elitismRate: Double = 0.01,
                    mutationRate: Double = 0.05,
                    maximumOffset: Double = 0.01,
                    populationSize: Int = 100,
                    boardSize: Int = 9,
                    difficulty: Difficulty = Difficulty.BASIC,
                    selectionFunction: Selection = Functions.Selection.FIT_RATE_SELECTION,
                    mutationFunction: Mutation = Functions.Mutation.SINGLE_CELL_MUTATION,
                    crossoverFunction: Crossover = Functions.Crossover.SINGLE_BLOCK_CROSSOVER
                   )
