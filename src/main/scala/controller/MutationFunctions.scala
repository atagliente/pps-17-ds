package controller

import model.{FittedSudokuChromosome, SudokuChromosome}

import scala.collection.mutable
import scala.util.Random

object MutationFunctions {

  def simpleCellMutation(availablePosByBlock: Map[Int, List[Int]], population: List[FittedSudokuChromosome], mutationRate: Double): List[FittedSudokuChromosome] = {
    val populationSize = population.length
    val n:Int = (mutationRate * populationSize).toInt
    val newPopulation = population.toBuffer
    (0 until n).foreach(f => {
      val rnd = Random.nextInt(populationSize)
      val ch: FittedSudokuChromosome = newPopulation (rnd)
      val genes = SudokuUtils.splitInBlock(ch.genes.toList).toBuffer
      val blockNumber = Random.nextInt(availablePosByBlock.size)
      val block = availablePosByBlock(blockNumber)
      val posA = block(Random.nextInt(block.size))
      val posB = block(Random.nextInt(block.size))
      val newGenes = genes (blockNumber).toBuffer
      val temp = genes (blockNumber)(posA)
      newGenes (posA) = genes (blockNumber)(posB)
      newGenes (posB) = temp
      genes (blockNumber) = newGenes.toList
      newPopulation (rnd) = FittedSudokuChromosome(SudokuUtils.blockToList (genes.toList), 0.0)
    })
    newPopulation.toList
  }

  def doubleCellsMutation(availablePosByBlock: Map[Int, List[Int]], population: List[FittedSudokuChromosome], mutationRate: Double): List[FittedSudokuChromosome] = {
    simpleCellMutation(availablePosByBlock, simpleCellMutation(availablePosByBlock, population, mutationRate), mutationRate)
  }

  def threeSwapMutation(availablePosByBlock: Map[Int, List[Int]], population: List[FittedSudokuChromosome], mutationRate: Double): List[FittedSudokuChromosome] = {
    val populationSize = population.length
    val n:Int = (mutationRate * populationSize).toInt
    val newPopulation = population.toBuffer
    (0 until n).foreach(f => {
      val rnd = Random.nextInt(populationSize)
      val ch: FittedSudokuChromosome = newPopulation (rnd)
      val genes = SudokuUtils.splitInBlock(ch.genes.toList).toBuffer
      val blockNumber = Random.nextInt(availablePosByBlock.size)
      val block:mutable.Stack[Int] = mutable.Stack().pushAll(availablePosByBlock(blockNumber))
      var posA = block.pop()
      var posB = block.pop()
      var posC = block.pop()
      val newGenes = genes (blockNumber).toBuffer
      newGenes (posA) = genes (blockNumber)(posC)
      newGenes (posB) = genes (blockNumber)(posA)
      newGenes (posC) = genes (blockNumber)(posB)
      genes (blockNumber) = newGenes.toList
      newPopulation (rnd) = FittedSudokuChromosome(SudokuUtils.blockToList (genes.toList), 0.0)
    })
    newPopulation.toList
  }

}
