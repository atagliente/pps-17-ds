package controller

/**
 * Functions to use for Genetic Algorithm solver
 */
object Functions extends Enumeration {

  object Selection extends Enumeration {
    type Selection = Value
    val ROULETTE_WHEEL_SELECTION:Value = Value("Roulette Wheel Selection")
    val RANDOM_SELECTION:Value = Value("Random Selection")
    val FIT_RATE_SELECTION:Value = Value("Fit Rate Selection")
  }

  object Mutation extends Enumeration {
    type Mutation = Value
    val DOUBLE_CELL_MUTATION:Value = Value("Double Cell Mutation")
    val SINGLE_CELL_MUTATION:Value = Value("Single Cell Mutation")
    val THREE_SWAP_MUTATION:Value = Value("Three Swap Mutation")
  }

  object Crossover extends Enumeration {
    type Crossover = Value
    val SINGLE_BLOCK_CROSSOVER:Value = Value("Single Block Crossover")
    val DOUBLE_BLOCKS_CROSSOVER:Value = Value("Double Blocks Crossover")
  }

}

