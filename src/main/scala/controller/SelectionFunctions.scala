package controller

import model.FittedSudokuChromosome

import scala.util
import scala.util.Random

object SelectionFunctions {

  def rouletteWheel(population: List[FittedSudokuChromosome], sudokuChromosome: FittedSudokuChromosome, maxOffset: Double): FittedSudokuChromosome = {
    val maxValue:Double = population.map(_.fitRate).sum
    var sumValue:Double = 0
    val threshold: Double = Random.nextDouble()*maxValue
    var fittedChromosome:FittedSudokuChromosome = population.head
    while (sumValue < threshold) {
      fittedChromosome = population(Random.nextInt(population.length))
      sumValue += fittedChromosome.fitRate
    }
    fittedChromosome
  }

  def byFitRate(population: List[FittedSudokuChromosome], sudokuChromosome: FittedSudokuChromosome, maxOffset: Double): FittedSudokuChromosome = {
    var chromosome:Option[FittedSudokuChromosome] = Option.empty
    var offset = maxOffset
    while (chromosome.isEmpty) {
      chromosome = population.find(sc => sc != sudokuChromosome && Math.abs(sudokuChromosome.fitRate - sc.fitRate) <= offset)
      offset += 0.01
    }
    chromosome.getOrElse(sudokuChromosome)
  }

  def randomSelection(population: List[FittedSudokuChromosome], sudokuChromosome: FittedSudokuChromosome, maxOffset: Double): FittedSudokuChromosome = {
    population(util.Random.nextInt(population.size))
  }

}