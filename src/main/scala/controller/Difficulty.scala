package controller

/**
 * Difficulty selection for default boards
 */
object Difficulty extends Enumeration {
  type Difficulty = Value
  val BASIC:Value = Value("Basic")
  val SIMPLE:Value = Value("Simple")
  val MEDIUM:Value = Value("Medium")
  val HARD:Value = Value("Hard")
  val EXTREME:Value = Value("Extreme")
  val CUSTOM:Value = Value("Custom")
}
