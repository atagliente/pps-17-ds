package controller

import model.SudokuGene


object BoardOperation {

  def valuesControl(line: Array[SudokuGene], valuesRange: Range): Double =
    (line.map(_.allele) intersect valuesRange).length.toDouble / valuesRange.size

  def blocksControl(board: List[List[SudokuGene]]):Double = {
    var blockCompletionRate:Double = 0
    for(x <- 0 until board.head.size) {
        blockCompletionRate += valuesControl(board(x).toArray, 1 to board.head.size)
    }
    blockCompletionRate / board.head.size
  }

  def boardCompletionRateWithInitialBoard(initialBoard: List[SudokuGene], board: Array[Array[SudokuGene]]): Double = {
    val values = board.flatten.toList
    val sValueNumber = initialBoard.count(_.allele != 0)
    var matchValue = 0.0
    (0 until initialBoard.size).foreach(i => {
      if(initialBoard(i).allele == values(i).allele) {
        matchValue += 1
      }
    })
    (matchValue / sValueNumber + boardCompletionRateWithoutBlocksControl(board)) / 2
  }

  def boardCompletionRateWithoutBlocksControl(board: Array[Array[SudokuGene]]): Double = {
    var rowsCompletionRate:Double = 0
    var columnCompletionRate:Double = 0.0
    var correctRows = board.length
    var correctColumns = board.head.length
    val tabulatedBoard = board.transpose
    for(i <- board.indices) {
      var lastRowColumnValue = valuesControl(board(i), 1 to board.length)
      if(lastRowColumnValue < 1) correctRows -= 1
      rowsCompletionRate += lastRowColumnValue
      lastRowColumnValue = valuesControl(tabulatedBoard(i), 1 to board.length)
      if(lastRowColumnValue < 1) correctColumns -= 1
      columnCompletionRate += lastRowColumnValue
    }
    rowsCompletionRate /= board.length
    columnCompletionRate /= board.head.length
    (rowsCompletionRate + columnCompletionRate) / 2
  }

}
