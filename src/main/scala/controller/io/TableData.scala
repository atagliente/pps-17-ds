package controller.io

/**
 * Table like data structure
 * @param header table header
 * @param rows table rows
 */
case class TableData(header: Option[List[String]], rows: List[List[String]])
