package controller.io

import java.io.{BufferedReader, BufferedWriter, FileReader, FileWriter}
import java.nio.file.{Path, Paths}

import com.opencsv

trait CSVFile extends File {

  /**
   * Get file writer
   * @return CSV writer
   */
  def writer: opencsv.CSVWriter

  /**
   * Get file reader
   * @return CSV file reader
   */
  def reader: opencsv.CSVReader
}

object CSVFile {

  def apply(fileName: String): CSVFileImpl = new CSVFileImpl(fileName)

  class CSVFileImpl(val fileName: String) extends CSVFile {

    override def path: Path = Paths.get(fileName)

    override def writer: opencsv.CSVWriter = new opencsv.CSVWriter(new BufferedWriter(new FileWriter(fileName, true)))

    override def reader: opencsv.CSVReader = new opencsv.CSVReader(new BufferedReader(new FileReader(fileName)))
  }

}
