package controller.io

import java.nio.file.Files

import scala.jdk.CollectionConverters._

trait CSVManager extends DataManager {
  override type TabulatedData = TableData
  override type FileSupport = CSVFile

  override def write(fileSupport: CSVFile, tabulatedData: TableData): Unit = {
    var temp = tabulatedData.rows
    if (!Files.exists(fileSupport.path))
      temp = tabulatedData.header.getOrElse(List.empty) +: tabulatedData.rows
    val writer = fileSupport.writer
    writer.writeAll(temp.map(_.toArray).asJava)
    writer.close()
  }

  override def read(fileSupport: CSVFile): Option[TableData] = {
    if (Files.exists(fileSupport.path)) {
      val reader = fileSupport.reader
      val content = reader.readAll().asScala
      Some(TableData(Some(content.head.toList), content.tail.toList.map(_.toList)))
    } else None
  }
}

object CSVManager {
  def apply(): CSVManager = new CSVManagerImpl()

  class CSVManagerImpl() extends CSVManager

}


