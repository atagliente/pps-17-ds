package controller.io

trait DataManager {
  type TabulatedData
  type FileSupport

  /**
   * Write `TabulatedData` type data in the specified `FileSupport`
   * @param fileSupport FileSupport
   * @param tabulatedData data
   */
  def write(fileSupport: FileSupport, tabulatedData: TabulatedData): Unit

  /**
   * Read `TabulatedData` from `FileSupport`
   * @param fileSupport FileSupport
   * @return None if file does not exists
   */
  def read(fileSupport: FileSupport): Option[TabulatedData]

}
