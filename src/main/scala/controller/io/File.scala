package controller.io

import java.nio.file.Path

trait File {

  /**
   *
   * @return File absolute path
   */
  def path: Path

}
