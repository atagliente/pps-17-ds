package controller

import model.FittedSudokuChromosome

import scala.collection.mutable.ListBuffer

trait GeneticEngine[T] {

  def selection(sudokuChromosomes: List[T], selectionFunction:(List[T], T , Double) => T, maxOffset: Double) : List[(T, T)]

  def crossover(selection: List[(T, T)], crossoverFunction: (T, T) => T) : List[T]

  def fitPopulation(population: List[T]) : List[T]

}

object GeneticEngine {

  def selection[T](sudokuChromosomes: List[T], selectionFunction:(List[T], T , Double) => T, maxOffset: Double)(implicit geneticEngine: GeneticEngine[T]) : List[(T, T)] = {
    geneticEngine.selection(sudokuChromosomes, selectionFunction, maxOffset)
  }

  def crossover[T](selection: List[(T, T)], crossoverFunction: (T, T) => T)(implicit geneticEngine: GeneticEngine[T]) : List[T] = {
    geneticEngine.crossover(selection, crossoverFunction)
  }

  def fitPopulation[T](population: List[T])(implicit geneticEngine: GeneticEngine[T]): List[T] = {
    geneticEngine.fitPopulation(population)
  }

}

object GeneticEngineImplicits {

  implicit object GeneticEngineSudoku extends GeneticEngine[FittedSudokuChromosome] {

    override def selection(sudokuChromosomes: List[FittedSudokuChromosome], selectionFunction: (List[FittedSudokuChromosome], FittedSudokuChromosome, Double) => FittedSudokuChromosome, maxOffset: Double): List[(FittedSudokuChromosome, FittedSudokuChromosome)] = {
      var selection:ListBuffer[(FittedSudokuChromosome, FittedSudokuChromosome)] = ListBuffer.empty
      sudokuChromosomes.foreach(sudokuChromosome => selection += Tuple2(sudokuChromosome, selectionFunction(sudokuChromosomes, sudokuChromosome, maxOffset)))
      selection.toList
    }

    override def crossover(selection: List[(FittedSudokuChromosome, FittedSudokuChromosome)], crossoverFunction: (FittedSudokuChromosome, FittedSudokuChromosome) => FittedSudokuChromosome): List[FittedSudokuChromosome] = {
      val newPopulation:ListBuffer[FittedSudokuChromosome] = ListBuffer.empty
      selection.foreach(couple => newPopulation += crossoverFunction(couple._1, couple._2))
      newPopulation.toList
    }

    override def fitPopulation(population: List[FittedSudokuChromosome] ): List[FittedSudokuChromosome] = {
      val fittedPopulation:ListBuffer[FittedSudokuChromosome] = ListBuffer.empty[FittedSudokuChromosome]
      population.foreach(ch => {
        val fitRate = BoardOperation.boardCompletionRateWithoutBlocksControl(SudokuUtils.sudokize (ch))
        fittedPopulation += FittedSudokuChromosome (ch.genes, fitRate)
      })
      fittedPopulation.toList
    }

  }

}

object Engine {

  def elitism(population: List[FittedSudokuChromosome], eliteRate: Double): (List[FittedSudokuChromosome], List[FittedSudokuChromosome]) = {
    val sortedPopulation = population.sortBy(_.fitRate)(Ordering.Double.TotalOrdering).reverse
    val elitePoint = (sortedPopulation.length*eliteRate).toInt
    val splittedPop = sortedPopulation.splitAt(elitePoint)
    (splittedPop._1, splittedPop._2)
  }

}
