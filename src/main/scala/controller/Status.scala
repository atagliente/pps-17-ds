package controller

import model.FittedSudokuChromosome

/**
 * Genetic Algorithm computation status as snapshot
 * @param leader current generation leader
 * @param avg current generation average fitness
 * @param gen current generation number
 * @param time elapsed time
 */
case class Status(leader: FittedSudokuChromosome, avg: Double, gen: Long, time: Long)

