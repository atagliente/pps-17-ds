package controller

import java.lang.Math.pow

import model.{Chromosome, FittedChromosome, FittedSudokuChromosome, SudokuChromosome, SudokuGene}

import scala.collection.mutable.ListBuffer
import scala.util.Random

object SudokuUtils {

  def sudokize(chromosome: FittedSudokuChromosome): Array[Array[SudokuGene]] = {
    val size = Math.pow(chromosome.genes.size, 0.5).toInt
    var board:Array[Array[SudokuGene]] = Array.ofDim(size, size)
    val iterator:Iterator[SudokuGene] = chromosome.genes.iterator
    for{row <- 0 until size; column <- 0 until size} {
      val sg:SudokuGene = iterator.next()
      board(row)(column) = sg
    }
    board
  }

  def generateRandomSudokuChromosome(possibleGenes: Range): SudokuChromosome = {
    def randomSudokuGene = SudokuGene(possibleGenes(Random.nextInt(possibleGenes.length)))
    SudokuChromosome(List.fill(math.pow(possibleGenes.max, 2).toInt)(randomSudokuGene))
  }

  def generateRandomSudokuChromosomeWithBlock(initialBoard:List[List[SudokuGene]], blockSize: Int): SudokuChromosome = {
    val size = Math.pow(blockSize, 2).toInt
    val board:ListBuffer[List[SudokuGene]] = ListBuffer.empty
    for(x <- 0 until size) {
      val notSelectable = initialBoard(x).map(_.allele)
      val selectable = List.range(1, size + 1).filterNot(notSelectable.contains(_))
      val iter = Random.shuffle(selectable).iterator
      var block:ListBuffer[SudokuGene] = ListBuffer.empty
      for(y <- 0 until size) {
        if(initialBoard(x)(y).allele == 0) block += SudokuGene(iter.next())
        else block += initialBoard(x)(y)
      }
      board += block.toList
    }
    SudokuChromosome(SudokuUtils.blockToList(board.toList))
  }

  def printSudokuBoard(sudokuChromosome: FittedSudokuChromosome): Unit = {
    sudokize(sudokuChromosome).foreach{row => row.map(g => s"${g.allele} ").foreach(print); println}
  }

  def splitInBlock[X](list: List[X]): List[List[X]] = {
    val sizeOfBlock = pow(list.size, 0.25).toInt
    val blocks = list.grouped(sizeOfBlock).toList
    val listOfBlock:ListBuffer[List[X]] = ListBuffer.empty[List[X]]
    var i = 0
    while(i < blocks.size) {
      var block:ListBuffer[List[X]] = ListBuffer.empty
      var inc = 0
      for (j <- 0 until sizeOfBlock) {
        block += blocks(i + inc)
        inc += sizeOfBlock
      }
      listOfBlock += block.flatten.toList
      if(listOfBlock.size % sizeOfBlock == 0) {
        i = i + (pow(sizeOfBlock, 2).toInt - sizeOfBlock)
      }
      i += 1
    }
    listOfBlock.toList
  }

  def blockToList[X](blocks: List[List[X]]): List[X] = {
    splitInBlock(blocks.flatten).flatten
  }

  def availablePositionByBlock(initialBoard: List[List[SudokuGene]], availableMinPos: Int) : Map[Int, List[Int]] = {
    var map:Map[Int, List[Int]] = Map.empty
    for (blockNumber <- 0 until initialBoard.size) {
      var list:ListBuffer[Int] = ListBuffer.empty
      for (posInBlock <- 0 until initialBoard(blockNumber).size) {
        if(initialBoard(blockNumber)(posInBlock).allele == 0)
          list += posInBlock
      }
      if(list.size >= availableMinPos) map += (blockNumber -> Random.shuffle(list.toList))
    }
    map
  }

  def forceBoard(initialBoard: List[SudokuGene], board: List[SudokuGene]): List[SudokuGene] = {
    var i = 0
    val forcedBoard:ListBuffer[SudokuGene] = ListBuffer().empty
    while(i < initialBoard.size) {
      if(initialBoard(i).allele != 0) forcedBoard += initialBoard(i)
      else forcedBoard += board(i)
      i+=1
    }
    forcedBoard.toList
  }

  def boardMatch(initializeBoard: List[SudokuGene], boardToTest: List[SudokuGene]): Boolean = {
    for(i <- 0 until initializeBoard.size) {
      if(initializeBoard(i).allele != 0 && initializeBoard(i).allele != boardToTest(i).allele) {
        return false
      }
    }
    true
  }

  def compareCromosomes(chromosomeA: SudokuChromosome, chromosomeB: SudokuChromosome): Int = {
    var counter = 0
    (0 until chromosomeA.genes.size).foreach(index => {
      if(chromosomeA.genes.toList(index).allele != chromosomeB.genes.toList(index).allele) counter += 1
    })
    counter
  }

}