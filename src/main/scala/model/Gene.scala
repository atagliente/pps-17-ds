package model

sealed trait Gene[T] {

  def allele: T
  
}

case class SudokuGene(allele: Int) extends Gene[Int]
