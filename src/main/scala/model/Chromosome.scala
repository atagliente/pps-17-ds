package model

trait Chromosome[T] {

  def genes: Iterable[Gene[T]]

}

case class SudokuChromosome(genes: Iterable[SudokuGene]) extends Chromosome[Int]