package model

trait Population[T] {

  def chromosomes: Iterable[Chromosome[T]]

}

case class SudokuPopulation(chromosomes: Iterable[SudokuChromosome]) extends Population[Int]