package model

sealed trait FittedChromosome[G] extends Chromosome[G]{

  def fitRate: Double

}

case class FittedSudokuChromosome(genes: Iterable[SudokuGene], fitRate: Double) extends FittedChromosome[Int]