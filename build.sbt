name := "pps-17-ds"

version := "0.1"

scalaVersion := "2.13.0"

libraryDependencies += "org.scala-lang.modules" %% "scala-swing" % "2.1.1"
libraryDependencies += "de.sciss" %% "scala-chart" % "0.7.1"
libraryDependencies += "com.opencsv" % "opencsv" % "4.6"


libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"

scalacOptions += "-deprecation"

Compile/mainClass := Some("Main")
mainClass in assembly := Some("Main")
target in assembly := file("target/libs/")
assemblyJarName in assembly := name.value + "_jar.jar"
test in assembly := {}