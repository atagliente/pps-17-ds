package algorithm.genetic.chromosomes.genes

trait Phenotype[PhenotypeTypology] {

  def information: PhenotypeTypology

}

object Phenotype {

  def oneIntegerPossibleValues: Range = 1 to 9

}
