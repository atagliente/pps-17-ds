package algorithm.genetic.chromosomes.genes

class OneIntegerValuePhenotype(val _information: Int) extends Phenotype[Int] {

  private var _content: Int = _information

  def this() {
    this(scala.util.Random.nextInt(Phenotype.oneIntegerPossibleValues.max) + 1)
  }

  override def information: Int = _content

}

