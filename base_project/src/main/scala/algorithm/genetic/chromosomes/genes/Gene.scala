package algorithm.genetic.chromosomes.genes

import scala.util.Random

trait Gene[GeneticPhenotype] {

  def mutationRate: Double
  def phenotype: GeneticPhenotype
  def mutate:Boolean = Random.nextInt(100) < (mutationRate * 100)

}
