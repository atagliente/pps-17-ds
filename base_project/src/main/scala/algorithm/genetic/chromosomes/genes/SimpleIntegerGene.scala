package algorithm.genetic.chromosomes.genes

import scala.collection.mutable.ListBuffer

class SimpleIntegerGene(_phenotype: OneIntegerValuePhenotype, _mutationRate: Double) extends Gene[OneIntegerValuePhenotype] {

  private val _mRate:Double = _mutationRate

  private val phenotypeInformation =  {
      var information:OneIntegerValuePhenotype = _phenotype
      if(mutate) {
          information = new OneIntegerValuePhenotype()
      }
      information
  }

  def this() {
    this(new OneIntegerValuePhenotype(), 0.01)
  }

  def this(_mutationRate: Double) {
    this(new OneIntegerValuePhenotype(), _mutationRate)
  }

  override def mutationRate: Double = _mutationRate

  override def phenotype: OneIntegerValuePhenotype = _phenotype

  override def toString: String = "" + phenotype

}
