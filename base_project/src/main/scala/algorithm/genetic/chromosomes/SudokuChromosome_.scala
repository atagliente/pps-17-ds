package algorithm.genetic.chromosomes

import algorithm.genetic.chromosomes.genes.SimpleIntegerGene

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer
import scala.util.Random

class SudokuChromosome_(initialGenes: List[SimpleIntegerGene], fitFunction: (SudokuChromosome_) => Double, mutationGeneRate:Double) extends Chromosome[SimpleIntegerGene] {

  private val _genes = initialGenes

  private var _fitRate:Double = 0

  private val  _fitFunction = fitFunction

  def this(fitFunction: (SudokuChromosome_) => Double, mutationGeneRate: Double) {
    this(Seq.fill(81)(new SimpleIntegerGene(mutationGeneRate)).toList, fitFunction, mutationGeneRate)
  }

  override def genes: List[SimpleIntegerGene] = _genes

  override def fitRate: Double = _fitRate

  override def toString: String = {
    var str:String =""
    str = "\n FIT RATE: " + fitRate
    for (i <- 0 to 80) {
      if(i % 9 == 0) {
        str = str + "\n"
      }
      if(i % 27 == 0) {
        str = str + "-----------------------------\n"
      }
      if(i % 3 == 0) {
        str = str + " | "
      }
      str = str + " " + genes(i).phenotype.information
    }
    str
  }

  override def fit(): Unit = _fitRate = _fitFunction(this)

  def integerGeneList: List[Int] = {
    var list:ListBuffer[Int] = ListBuffer()
    genes.foreach(g => {
      list += g.phenotype.information
    })
    list.toList
  }

}
