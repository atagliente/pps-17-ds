package algorithm.genetic.chromosomes

import scala.collection.immutable.List

trait Chromosome[GeneType] {

  def genes: List[GeneType]
  def fitRate: Double
  def fit(): Unit

}