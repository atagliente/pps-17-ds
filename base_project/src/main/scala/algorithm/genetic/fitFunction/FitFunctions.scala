package algorithm.genetic.fitFunction

import algorithm.genetic.chromosomes.SudokuChromosome_
import algorithm.genetic.chromosomes.genes.Phenotype

import scala.collection.immutable.List

object FitFunctions {

  def simpleIntegerMatching(a: List[Int], b: List[Int]): Double = {
    val aS = a.sorted
    val bS = b.sorted
    var matches:Double = 0;
    for (i <- 0 to (aS.length - 1)) {
      if (aS(i) == bS(i)) {
        matches = matches + 1;
      }
    }
    matches / aS.length
  }

  def sudoku(chromosome: SudokuChromosome_): Double = {
    (controlSudokuBlocks(chromosome) + rowsControl(chromosome) + columnsControl(chromosome)) / 3
  }

  def rowsControl(chromosome: SudokuChromosome_): Double = {
    var matches:Double = 0
    for (i <- 0 to 72 by 9) {
      matches = matches + simpleIntegerMatching(chromosome.integerGeneList.slice(i, i + 8), Phenotype.oneIntegerPossibleValues.toList)
    }
    matches / 9
  }

  def columnsControl(chromosome: SudokuChromosome_): Double = {
    var matches:Double = 0
    for (i <- 0 to 8) {
      var column:List[Int] = List(chromosome.genes(i).phenotype.information,
                                  chromosome.genes(i+9).phenotype.information,
                                  chromosome.genes(i+18).phenotype.information,
                                  chromosome.genes(i+27).phenotype.information,
                                  chromosome.genes(i+36).phenotype.information,
                                  chromosome.genes(i+45).phenotype.information,
                                  chromosome.genes(i+54).phenotype.information,
                                  chromosome.genes(i+63).phenotype.information,
                                  chromosome.genes(i+72).phenotype.information)
      matches = matches + simpleIntegerMatching(column, Phenotype.oneIntegerPossibleValues.toList)
    }
    matches / 9
  }

  def controlSudokuBlocks(a: SudokuChromosome_): Double = {
    var matches:Double = 0
    for (i <- 0 to 2) {
      var x = 27 * i
      var oneDPos:Int = 0
      for (i <- 0 to 2) {
        var y = 3 * i
        oneDPos = x + y
        val block = List(a.genes(oneDPos).phenotype.information,
          a.genes(oneDPos + 1).phenotype.information,
          a.genes(oneDPos + 2).phenotype.information,
          a.genes(oneDPos + 9).phenotype.information,
          a.genes(oneDPos + 10).phenotype.information,
          a.genes(oneDPos + 11).phenotype.information,
          a.genes(oneDPos + 18).phenotype.information,
          a.genes(oneDPos + 19).phenotype.information,
          a.genes(oneDPos + 20).phenotype.information,
        ).sorted
        matches = matches + simpleIntegerMatching(block, Phenotype.oneIntegerPossibleValues.toList)
      }
    }
    matches / 9
  }

  def returnOne(a: SudokuChromosome_): Double = {
    1
  }

}
