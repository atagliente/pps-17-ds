package algorithm.genetic

import algorithm.genetic.chromosomes.SudokuChromosome_

import scala.collection.immutable.List

class SudokuPopulation(_initialPopulation: List[SudokuChromosome_]) extends Population[SudokuChromosome_] {

  private var _population: List[SudokuChromosome_] = _initialPopulation

  private var _leader = None: Option[SudokuChromosome_]

  private var _avgFitness:Double = 0

  override def leader: Option[SudokuChromosome_] = _leader

  override def avgFitnessRate: Double = _avgFitness

  override def population: List[SudokuChromosome_] = _population

  override def fitAllPopulation(): Unit = {
    var sum:Double = 0
    population.foreach(c => {
      c.fit()
      sum = sum + c.fitRate
      if(leader.isEmpty || c.fitRate > leader.get.fitRate) {
        _leader = Option(c)
      }
    })
    _avgFitness = sum / population.length
  }

  override def toString: String = {
    var str = "POPULATION:\n"
    population.foreach(c => {
      str = str + "\t" + c.toString + "\n"
    })
    str
  }

}
