package algorithm.genetic

import algorithm.genetic.chromosomes.Chromosome

import scala.collection.immutable.List

trait Population[ChromosomeType] {

    def population: List[ChromosomeType]

    def fitAllPopulation(): Unit

    def leader: Option[ChromosomeType]

    def avgFitnessRate: Double

}
