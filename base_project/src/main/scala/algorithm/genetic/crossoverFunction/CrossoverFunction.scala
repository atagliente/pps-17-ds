package algorithm.genetic.crossoverFunction

import algorithm.genetic.chromosomes.SudokuChromosome_
import algorithm.genetic.chromosomes.genes.{OneIntegerValuePhenotype, SimpleIntegerGene}

import scala.collection.mutable.ListBuffer
import scala.util.Random

object CrossoverFunction {

  def singePointCrossoverFunction[Gene](parentA: List[Gene], parentB: List[Gene]): List[Gene] = {
    var chromosomes:ListBuffer[Gene] = ListBuffer()
    val crossoverPoint = Random.nextInt(parentA.length)
    for (i <- 0 to parentA.length - 1) {
      if(i <= crossoverPoint) {
        chromosomes += parentA(i)
      } else {
        chromosomes += parentB(i)
      }
    }
    chromosomes.toList
  }

  def singePointCrossoverFunctionForSudoku(parentA: SudokuChromosome_, parentB: SudokuChromosome_, fitFunction: (SudokuChromosome_) => Double, mutationRate: Double): SudokuChromosome_ = {
    var newChromosomeGenes:ListBuffer[SimpleIntegerGene] = ListBuffer()
    val crossoverPoint = Random.nextInt(parentA.genes.length)
    for (i <- 0 to parentA.genes.length - 1) {
      if(i <= crossoverPoint) {
        newChromosomeGenes += new SimpleIntegerGene(parentA.genes(i).phenotype, mutationRate)
      } else {
        newChromosomeGenes += new SimpleIntegerGene(parentB.genes(i).phenotype, mutationRate)
      }
    }
    new SudokuChromosome_(newChromosomeGenes.toList, fitFunction, mutationRate)
  }

}
