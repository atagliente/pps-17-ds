package algorithm.genetic.utility

import algorithm.genetic.SudokuPopulation
import algorithm.genetic.chromosomes.SudokuChromosome_
import algorithm.genetic.chromosomes.genes.{OneIntegerValuePhenotype, SimpleIntegerGene}
import algorithm.genetic.crossoverFunction.CrossoverFunction
import algorithm.genetic.parentSelection.ParentSelection

import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

object Utility {

  def generateSudokuChromosomeByList(listOfInt: List[Int], mutationRate: Double, fitFunction: (SudokuChromosome_) => Double): SudokuChromosome_ = {
    val parentAGenes: ListBuffer[SimpleIntegerGene] = ListBuffer()
    listOfInt.foreach(v => {
      parentAGenes += new SimpleIntegerGene(new OneIntegerValuePhenotype(v), mutationRate)
    })
    new SudokuChromosome_(parentAGenes.toList, fitFunction, mutationRate)
  }

  def generateRandomSudokuPopulation(size:Int, mutationRate:Double, fitFunction: (SudokuChromosome_) => Double): SudokuPopulation = {
    var population:ListBuffer[SudokuChromosome_] = ListBuffer()
    for (i <- 1 to size) {
      population += new SudokuChromosome_(fitFunction, mutationRate)
    }
    new SudokuPopulation(population.toList)
  }

  def partnerNumberFunction(fitnessRate: Double): Int = {
    val functionResult: Double = 1/(1 + Math.pow(2.718281, (-(fitnessRate-0.3)*4)))
    val maxPartner:Int = 3
    (maxPartner * functionResult).toInt
  }

  def evolve(population: SudokuPopulation, partnerFunction: (Double) => Int, delta:Double, fitFunction: (SudokuChromosome_) => Double, mutationRate:Double): SudokuPopulation = {
    var newGeneration:ListBuffer[SudokuChromosome_] = ListBuffer()
    population.population.foreach(c => {
      ParentSelection.rouletteWheelSelection(c, population.population, partnerFunction, delta)
                      .foreach(partner => {
                        newGeneration += CrossoverFunction.singePointCrossoverFunctionForSudoku(c, partner, fitFunction, mutationRate)
                      })
    })
    new SudokuPopulation(newGeneration.toList)
  }

  def thanos(population:SudokuPopulation, rate:Double): SudokuPopulation = {
    var newWorld:List[SudokuChromosome_] = population.population.sortBy(_.fitRate)(Ordering[Double].reverse)
                                                                .slice(0, (population.population.length*rate).toInt)
    new SudokuPopulation(newWorld)
  }

}
