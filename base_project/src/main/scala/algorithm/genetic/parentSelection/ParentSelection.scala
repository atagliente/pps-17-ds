package algorithm.genetic.parentSelection

import algorithm.genetic.chromosomes.{Chromosome, SudokuChromosome_}

import scala.collection.mutable.ListBuffer
import scala.util.Random

object ParentSelection {

  def rouletteWheelSelection(chromosome:SudokuChromosome_, chromosomes: List[SudokuChromosome_], partnersNumberFunction:(Double) => Int, delta: Double): List[SudokuChromosome_] = {
    var chosedParents:ListBuffer[SudokuChromosome_] = ListBuffer()
    val speedDateNumber:Int = partnersNumberFunction(chromosome.fitRate)
    val chromosomesSize:Int = chromosomes.length
    for(i <- 1 to speedDateNumber) {
      var iphoteticPartner:SudokuChromosome_ = chromosomes(Random.nextInt(chromosomesSize))
      val deltaCompare:Double = Math.abs(iphoteticPartner.fitRate - chromosome.fitRate)
      while (iphoteticPartner == chromosome) { 
        iphoteticPartner = chromosomes(Random.nextInt(chromosomesSize))
      }
      chosedParents += iphoteticPartner
    }
    chosedParents.toList
  }

  def rouletteWheelSingleSelection(chromosome:SudokuChromosome_, chromosomes: List[SudokuChromosome_], partnersNumberFunction:(Double) => Int, delta: Double): List[SudokuChromosome_] = {
    var chosedParents:ListBuffer[SudokuChromosome_] = ListBuffer()
    val chromosomesSize:Int = chromosomes.length
    var iphoteticPartner: SudokuChromosome_ = chromosomes(Random.nextInt(chromosomesSize))
    while (iphoteticPartner == chromosome || (Math.abs(iphoteticPartner.fitRate - chromosome.fitRate) > delta)) {
       iphoteticPartner = chromosomes(Random.nextInt(chromosomesSize))
    }
    chosedParents += iphoteticPartner
    chosedParents.toList
  }

}
