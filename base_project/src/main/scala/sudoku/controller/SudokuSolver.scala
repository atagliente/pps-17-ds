package sudoku.controller

abstract class SudokuSolver {

  def solve(): Array[Array[Int]]

}

object SudokuSolver {

  implicit val geneticSolver = new SudokuSolver {
      override def solve():  Array[Array[Int]] = new Array[Array[Int]](2)
  }

}