package sudoku.controller

import scala.util.Random

object BoardOperation {

  def createRandomBoard(x:Int, y:Int, populateFunction: () => Int): IndexedSeq[IndexedSeq[Int]] = Array.fill(x)(Array.fill(y)(populateFunction()).toIndexedSeq).toIndexedSeq

  def printBoard(board: IndexedSeq[IndexedSeq[Int]]): Unit = board.foreach(rows => {
                                                                            print("\n")
                                                                            rows.foreach(cell=>print(cell + " "))
                                                                          })

  def blockControl(block: IndexedSeq[IndexedSeq[Int]], valuesRange: Range): Double =  {
    (block.flatten intersect valuesRange).size.toDouble / valuesRange.size
  }

  def rowOrColumnControl(line: IndexedSeq[Int], valuesRange: Range): Double =  {
    //print("\n\n" + line)
    (line intersect valuesRange).size.toDouble / valuesRange.size
  }

  def boardCompletionRate(board: IndexedSeq[IndexedSeq[Int]]): Double = {
    var rowsCompletionRate:Double = 0
    val rowSize = board.size
    val columnSize = board(0).size
    val xInc = Math.sqrt(rowSize).toInt
    val yInc = Math.sqrt(columnSize).toInt
    val valueRange = 1 to (xInc * yInc)
    var columnCompletionRate:Double = 0.0
    //Rows and Columns control
    for(i <- 0 until rowSize) {
      rowsCompletionRate += rowOrColumnControl(board(0), valueRange)
      columnCompletionRate += rowOrColumnControl(board.flatMap(rowCell => List(rowCell(i))), valueRange)
    }
    rowsCompletionRate /= rowSize
    columnCompletionRate /= columnSize
    //Block control
    var blockCompletionRate:Double = 0
    for(x <- 0 to rowSize - xInc by xInc) {
      for(y <- 0 to columnSize - yInc by yInc) {
        blockCompletionRate += blockControl(board.slice(x, x + xInc).map(x => x.slice(y, y + yInc)), valueRange)
      }
    }
    blockCompletionRate /= (xInc * yInc)
    (rowsCompletionRate + columnCompletionRate + blockCompletionRate) / 3
  }

}
