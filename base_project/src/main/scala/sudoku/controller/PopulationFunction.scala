package sudoku.controller

import scala.util.Random

object PopulationFunction {

  def sudokuCellPopulator(): Int = Random.nextInt(9) + 1

}
