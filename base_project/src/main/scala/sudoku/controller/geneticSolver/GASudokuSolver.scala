package sudoku.controller.geneticSolver

import sudoku.controller.BoardOperation

import scala.collection.mutable.ListBuffer
import scala.util.Random

object GASudokuSolver extends GAoperations[IndexedSeq[IndexedSeq[Int]]] {

  override def populate(numberOfElements: Int, populateFunction: () => IndexedSeq[IndexedSeq[Int]]): List[IndexedSeq[IndexedSeq[Int]]] = {
    List.fill(numberOfElements)(populateFunction())
  }

  override def crossover(parentsList: List[(IndexedSeq[IndexedSeq[Int]], List[IndexedSeq[IndexedSeq[Int]]])],
                         crossoverFunction: (IndexedSeq[IndexedSeq[Int]], IndexedSeq[IndexedSeq[Int]]) => IndexedSeq[IndexedSeq[Int]]):
                         List[IndexedSeq[IndexedSeq[Int]]] = {
    parentsList.flatMap(parents => {
      parents._2.map(parent => crossoverFunction(parents._1, parent))
    })
  }

  override def selection(population: List[IndexedSeq[IndexedSeq[Int]]],
                         partnerSelectionFunction: (List[IndexedSeq[IndexedSeq[Int]]]) => List[IndexedSeq[IndexedSeq[Int]]]):
                         List[(IndexedSeq[IndexedSeq[Int]], List[IndexedSeq[IndexedSeq[Int]]])] = {
    val selection = new ListBuffer[(IndexedSeq[IndexedSeq[Int]], List[IndexedSeq[IndexedSeq[Int]]])]
    population.foreach(cell => {
      selection += Tuple2(cell, partnerSelectionFunction(population))
    })
    return selection.toList
  }

}
