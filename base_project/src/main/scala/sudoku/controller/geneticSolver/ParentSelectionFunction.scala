package sudoku.controller.geneticSolver

import scala.util.Random

object ParentSelectionFunction {

  def randomWheelSelection(population: List[IndexedSeq[IndexedSeq[Int]]]) : List[IndexedSeq[IndexedSeq[Int]]] = {
    List(population(new Random().nextInt(population.length)))
  }

}
