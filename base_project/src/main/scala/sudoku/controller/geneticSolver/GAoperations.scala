package sudoku.controller.geneticSolver

import algorithm.genetic.Population

trait GAoperations[Chromosome]{

  def populate(numberOfElements: Int, populateFunction: () => Chromosome): List[Chromosome]

  def selection(population: List[Chromosome],
                partnerSelectionFunction: (List[Chromosome]) => List[Chromosome],
                ): List[Tuple2[Chromosome, List[Chromosome]]]

  def crossover(parentsList: List[Tuple2[Chromosome, List[Chromosome]]],
                crossoverFunction: (Chromosome, Chromosome) => Chromosome): List[Chromosome]

  //def mutation(population: List[Chromosome], mutationRate: Double): List[Chromosome]

}
