package sudoku.controller.geneticSolver

import scala.util.Random

object CrossoverFunctions {

  def singlePointCrossover(parentA: IndexedSeq[IndexedSeq[Int]], parentB: IndexedSeq[IndexedSeq[Int]]): IndexedSeq[IndexedSeq[Int]] = {
    val crossoverPoint = new Random().nextInt(parentA.size - 1)
    IndexedSeq(parentA.splitAt(crossoverPoint)._1, parentB.splitAt(crossoverPoint)._2).flatten
  }

}
