package sudoku.controller

import algorithm.genetic.parentSelection.ParentSelection
import javafx.collections.transformation.SortedList
import org.scalatest.FunSuite
import sudoku.controller.geneticSolver.{CrossoverFunctions, GASudokuSolver, ParentSelectionFunction}

import scala.collection.mutable.ListBuffer
import scala.util.Random

class BoardOperationTest extends FunSuite {

  test("testCreateRandomBoard") {
      val board = BoardOperation.createRandomBoard(9, 9, PopulationFunction.sudokuCellPopulator)
      BoardOperation.printBoard(board)
      print("\n\n" + BoardOperation.boardCompletionRate(board))
      var map = Map()
      var population = GASudokuSolver.populate(100000, () => BoardOperation.createRandomBoard(9,9, PopulationFunction.sudokuCellPopulator))
//      population.foreach(x => {
//                          BoardOperation.printBoard(x)
//                          print("\n")
//                        })
      var leader = (, 0.0)
      var sortPopulation = new ListBuffer[(IndexedSeq[IndexedSeq[Int]], Double)]
      while(leader._2 < 1) {
        val parents = GASudokuSolver.selection(population, ParentSelectionFunction.randomWheelSelection)
        population = GASudokuSolver.crossover(parents, CrossoverFunctions.singlePointCrossover)
        population.foreach(x => {
          sortPopulation += Tuple2(x, BoardOperation.boardCompletionRate(x))
        })
        sortPopulation.sortBy(_._2)
        leader = sortPopulation.head
      }

  }

}
