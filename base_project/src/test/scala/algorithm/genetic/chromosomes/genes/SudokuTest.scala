package algorithm.genetic.chromosomes.genes

import algorithm.genetic.SudokuPopulation
import algorithm.genetic.fitFunction.FitFunctions
import algorithm.genetic.utility.Utility
import org.scalatest.FunSuite

class SudokuTest extends FunSuite{
  test("POPULATION CREATION TEST!") {
    var population:SudokuPopulation = Utility.generateRandomSudokuPopulation(500, 0.01 ,FitFunctions.sudoku)
    //print(population)
    //population.fitAllPopulation()
    //print(population)
    var i:Int = 0
    var delta:Double=0.1
    while (population.leader.isEmpty || population.leader.get.fitRate == 1) {
      population.fitAllPopulation()
      i = i + 1
      print("\nGEN " + i + " POPULATION SIZE: " + population.population.length + " AVG FITNESS RATE: " + population.avgFitnessRate + " DELTA: " + delta)
      print("\nLEADER:\n" + population.leader.get + "\n")
      population = Utility.evolve(population, Utility.partnerNumberFunction, delta , FitFunctions.sudoku, 0.01)
      if(population.population.length > 2000) {
        print("\n\n\t\tTHANOS: Perfectly balanced, as all things should be...\n\n")
        population = Utility.thanos(population, 0.25)
      }
    }
    print(population.leader.get)
  }
}
