package algorithm.genetic.chromosomes.genes

import algorithm.genetic.chromosomes.SudokuChromosome_
import algorithm.genetic.crossoverFunction.CrossoverFunction
import algorithm.genetic.fitFunction.FitFunctions
import algorithm.genetic.utility.Utility
import org.scalatest.FunSuite

import scala.collection.immutable.List

class CrossoverFunctionTest extends FunSuite{

  test("Crossover function test") {
    val parentA:List[Int] = List(1,1,1,1,1,1,1,1,1)
    val parentB:List[Int] = List(3,3,3,3,3,3,3,3,3)
    val pCA:SudokuChromosome_ = Utility.generateSudokuChromosomeByList(parentA, 0.01, FitFunctions.returnOne)
    val pCB:SudokuChromosome_ = Utility.generateSudokuChromosomeByList(parentB, 0.01, FitFunctions.returnOne)
    print(new SudokuChromosome_(CrossoverFunction.singePointCrossoverFunction(pCA.genes, pCB.genes), FitFunctions.returnOne, 0.01))
  }

}
