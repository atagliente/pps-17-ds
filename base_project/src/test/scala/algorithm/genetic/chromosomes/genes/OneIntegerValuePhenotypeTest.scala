package algorithm.genetic.chromosomes.genes

import org.scalatest.FunSuite

import scala.util.Random

class OneIntegerValuePhenotypeTest extends FunSuite{
  test("Specific Phenotype Creation") {
    val rnd = Random.nextInt(Phenotype.oneIntegerPossibleValues.max)
    val oIVP = new OneIntegerValuePhenotype(rnd)
    assert(oIVP.information == rnd)
  }
}
