package algorithm.genetic.chromosomes.genes

import org.scalatest.FunSuite

class PhenotypeTest extends FunSuite{

  test("Print all values in range") {
    Phenotype.oneIntegerPossibleValues.foreach(x => print(x+" "))
  }
}
